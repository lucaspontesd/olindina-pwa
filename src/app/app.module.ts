import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NegociosComponent } from './negocios/negocios.component';
import { DadosComponent } from './dados/dados.component';
import { SintomasComponent } from './sintomas/sintomas.component';
import { SobreComponent } from './sobre/sobre.component';
import { MapaComponent } from './mapa/mapa.component';

import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { JwtModule } from '@auth0/angular-jwt';
import { AuthGuardService } from "./auth/auth-guard.service";
import { AuthService } from "./auth/auth.service";
import { NgSelect2Module } from 'ng-select2';

import { AgmCoreModule } from '@agm/core';
import { MenuComponent } from './menu/menu.component';
import { KzMaskDirective } from './utils/directive/kz-mask.directive';
import { OnCreate } from './utils/directive/oncreate.directive';
import { Select2Component } from './utils/select2/select2.component';
import { ModalComponent } from './utils/modal/modal.component';
import { AppRoutingModule } from './app.routing.module';
import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada/pagina-nao-encontrada.component';
import { NgxSpinnerModule } from "ngx-spinner";


export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ModalComponent,
    DadosComponent,
    SintomasComponent,
    SobreComponent,
    MapaComponent,
    MenuComponent,
    KzMaskDirective,
    OnCreate,
    Select2Component,
    NegociosComponent,
    PaginaNaoEncontradaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
    HttpModule,
    NgSelect2Module,
    GooglePlaceModule,
    AgmCoreModule.forRoot({
     apiKey: 'AIzaSyA-EGpNG_uYU7uKjy0F6AL6IHgyFk-lO-Q'
    }),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['example.com'],
        blacklistedRoutes: ['example.com/examplebadroute/']
      }
    }),
    AppRoutingModule
  ],
  providers: [
    ModalComponent,
    MenuComponent,
    AuthService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
