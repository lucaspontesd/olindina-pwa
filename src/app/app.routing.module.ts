import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada/pagina-nao-encontrada.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NegociosComponent } from './negocios/negocios.component';
import { DadosComponent } from './dados/dados.component';
import { SintomasComponent } from './sintomas/sintomas.component';
import { SobreComponent } from './sobre/sobre.component';
import { MapaComponent } from './mapa/mapa.component';

import { AuthGuardService } from "./auth/auth-guard.service";


const appRoutes: Routes = [
    // { path: 'dashboard', component: DashBoardComponent, canActivate: [AuthGuardService] },
    // { path: 'perfil', component: PerfilComponent, canActivate: [AuthGuardService] },
    // { path: 'sobre', component: SobreComponent, canActivate: [AuthGuardService] },
    // { path: 'desafios', component: DesafiosComponent, canActivate: [AuthGuardService] },
    // { path: 'risco', component: RiscoComponent, canActivate: [AuthGuardService] },
    // { path: 'equipamentos', component: EquipamentosComponent, canActivate: [AuthGuardService] },
    // { path: 'noticias', component: NoticiasComponent, canActivate: [AuthGuardService] },
    // { path: 'ranking', component: RankingComponent, canActivate: [AuthGuardService] },
    // { path: 'pontuacao', component: PontuacaoComponent, canActivate: [AuthGuardService] },

    //{ path: 'naoEncontrado', component: CursoNaoEncontradoComponent },
    
    { path: 'mapa', component: MapaComponent},
    { path: 'negocios', component: NegociosComponent},
    { path: 'sobre', component: SobreComponent},
    { path: 'sintomas', component: SintomasComponent},
    { path: 'dados', component: DadosComponent},
    { path: 'home', component: HomeComponent},
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: '**', component: PaginaNaoEncontradaComponent } //, canActivate: [AuthGuard]}
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule {}
