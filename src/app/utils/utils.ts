declare var $,document;
export class Utils {
  constructor() {

  }
  startTutorial(id,icone,message,titulo,passo){
    return new Promise ((resolve, reject)=>{

      let bkg:any = document.createElement("div");
      bkg.setAttribute("class","tuto-bkg");



      let htmlHeight:any = document.querySelector("html");
      htmlHeight.scrollTop = 0;
      var h = htmlHeight.getBoundingClientRect().height;
      var body = document.body, html = document.documentElement;
      var height = Math.max( body.scrollHeight, body.offsetHeight, 
                            html.clientHeight, html.scrollHeight, html.offsetHeight );
      console.log(h)
      bkg.style.height = height+"px";

      var element = document.querySelector(id);
      var position = element.getBoundingClientRect();
      if((position.top/2)<0){
        htmlHeight.scrollTop = 0;
      }else{
        htmlHeight.scrollTop = (position.top/2);
      }

      // setTimeout(()=>{

        // position = element.getBoundingClientRect();
        let button = this.createButton(icone,message);
        button.style.width = position.width+"px";
        button.style.height = position.height+"px";
        button.style.position = "absolute";
        button.style.top = position.top+"px";
        button.style.left = position.left+"px";
        button.addEventListener('click',()=>{

          resolve(0);
        });

        let tittle = this.createTittle(titulo);
        tittle.style.top = (position.top/1.3)+"px";
        let msg = this.createMsg(passo);
        msg.style.left = (position.width+position.left+10)+"px";
        msg.style.top = position.top+"px";

        bkg.appendChild(button);
        bkg.appendChild(tittle);
        bkg.appendChild(msg);

        document.querySelector("body").appendChild(bkg);



      // },500);
    });


  }
  removeTutorial(){
    document.querySelector(".tuto-bkg").outerHTML="";
  }
  createButton(icon,titulo){
    let btn:any=document.createElement("button");
    btn.innerHTML=icon+titulo;
    btn.setAttribute("class","btn-flat waves-effect white btn-tuto");
    return btn;
  }
  createTittle(txt){
    let div = document.createElement("div");
    div.setAttribute("class","tuto-tittle");
    div.innerHTML=txt;
    return div;
  }
  createMsg(msg){
    let div = document.createElement("div");
    div.setAttribute("class","tuto-msg");
    div.innerHTML=msg;
    return div;
  }
}
