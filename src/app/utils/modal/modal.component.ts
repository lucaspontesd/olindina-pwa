import {Component,OnInit,Input } from '@angular/core';

@Component({
  selector: 'modal',
  templateUrl: 'modal.component.html',
  styleUrls: ['modal.component.css']
})
export class ModalComponent {
  @Input() name: string;
  constructor() {
  }
  ngOnInit(){
    let dom = document.querySelector("modal[data-component='modal']").getAttribute("dom");
    document.getElementById("content").innerHTML = dom;

    var modalStyle:any = document.querySelector("modal[data-component='modal']").childNodes[0];
    document.querySelector("[data-modal-vd='open']").addEventListener("click",()=>{
      modalStyle.setAttribute("modal-vd","open");
    });
    document.querySelector("[data-modal-vd='close']").addEventListener("click",()=>{
      modalStyle.setAttribute("modal-vd","close");
    });
  }

  ngOnChange(){
    console.log("MARILENE");
  }

}
