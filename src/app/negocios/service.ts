import { Headers, RequestOptions } from '@angular/http';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { AlertDom } from '../utils/alert/alert';
import { Values } from '../utils/values';
import { Session } from '../utils/session';

declare var jquery:any;
declare var $ :any;
declare var Materialize:any;


export class NegociosService {
    http:any;
    alert:any;
    session:Session = new Session();
    constructor(http) {
      this.http = http;
      this.alert = new AlertDom();
    }

    sendNegocio(negocio,callback){
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append("Authorization","Bearer "+this.session.getSession("vm_tk"));

      console.log(negocio);

      this.http.post(Values.URL+"negocio",JSON.stringify(negocio),{
        headers: headers}
      ).subscribe(res => {
        console.log(res.status);
        let result = res.json();
        console.log("negocio",res.json());


        if(!result.error){
          this.alert.openMsg(result.message,3000,"sucesso");
          callback(true);
        }else{
          this.alert.openMsg(result.message,3000,"erro");
          callback(false);
        }

       }, (err) => {
         console.log(err);
        //  let resp = JSON.parse(err._body);
         this.alert.openMsg("Ocorreu um erro interno.",3000,"erro");
         callback(false);
       });
    }
    editNegocio(negocio,callback){
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append("Authorization","Bearer "+this.session.getSession("vm_tk"));
      console.log(negocio);
      this.http.put(Values.URL+"editNegocio",JSON.stringify(negocio),{
        headers: headers}
      ).subscribe(res => {
        console.log(res.status);
        let result = res.json();
        console.log("negocio",res.json());
        if(!result.error){
          this.alert.openMsg(result.message,3000,"sucesso");
          callback(true);
        }else{
          this.alert.openMsg(result.message,3000,"erro");
          callback(false);
        }

       }, (err) => {
         console.log(err);
        //  let resp = JSON.parse(err._body);
         this.alert.openMsg("Ocorreu um erro interno.",3000,"erro");
         callback(false);
       });
    }
    meuNegocio(callback){
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append("Authorization","Bearer "+this.session.getSession("vm_tk"));

      this.http.get(Values.URL+"meuNegocio",{
        headers: headers}
      ).subscribe(res => {
        var resp = JSON.parse(res._body);
        console.log(resp);
        if(!resp.error){
          callback(resp.result);
        }else{
          callback(false);
        }
       }, (err) => {
         console.log(err);
         this.alert.openMsg("Ocorreu um erro interno.",3000,"erro");
         callback(false);
       });
    }

    listNegocio(lat,long,callback){
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      this.http.get(Values.URL+"listNegocio/"+lat+"/"+long,{
        headers: headers}
      ).subscribe(res => {
        var result = JSON.parse(res._body);
        console.log(result);
        if(!result.error){
          callback(result.lista);
        }else{
          callback(false);
        }
       }, (err) => {
         console.log(err);
        //  let resp = JSON.parse(err._body);
         this.alert.openMsg("Ocorreu um erro interno.",3000,"erro");
         callback(false);
       });
    }

    filtroNegocio(lat,long,filter,callback){
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      this.http.get(Values.URL+"filtroNegocio/"+lat+"/"+long+"/"+filter,{
        headers: headers}
      ).subscribe(res => {
        var result = JSON.parse(res._body);
        console.log(result);
        if(!result.error){
          callback(result.lista);
        }else{
          callback(false);
        }
       }, (err) => {
         console.log(err);
        //  let resp = JSON.parse(err._body);
         this.alert.openMsg("Ocorreu um erro interno.",3000,"erro");
         callback(false);
       });
    }

    
    

}
