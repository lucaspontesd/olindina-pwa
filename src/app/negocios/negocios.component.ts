import { Component, OnInit, ViewChild } from '@angular/core';
import { Session } from '../utils/session';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route } from '@angular/router';
import { NegociosService } from './service';
import { AlertDom } from '../utils/alert/alert';
import { Http, Response } from '@angular/http';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

declare var jquery:any;
declare var $ :any;
declare var L;
declare var LeafIcon;
declare var window :any;
declare var SimpleBar:any;
declare var google:any;
declare var navigator:any;

@Component({
  selector: 'app-negocios',
  templateUrl: './negocios.component.html',
  styleUrls: ['./negocios.component.css']
})
export class NegociosComponent implements OnInit {
  alert = new AlertDom();
  categorias:any = [
    {value:0,name:"Todas"},
    {value:1,name:"Restaurante"},
    {value:2,name:"Lanchonete"},
    {value:3,name:"Salão de Beleza"},
    {value:4,name:"Bares e Bebida"},
    {value:5,name:"Serviços em Geral"},
    {value:6,name:"Assistência técnica"},
    {value:6,name:"Serviços domésticos"},
  ];
  categoriasFilter:any = [
    {value:1,name:"Restaurante"},
    {value:2,name:"Lanchonete"},
    {value:3,name:"Salão de Beleza"},
    {value:4,name:"Bares e Bebida"},
    {value:5,name:"Serviços em Geral"},
    {value:6,name:"Assistência técnica"},
    {value:6,name:"Serviços domésticos"},
  ];
  filtroSelect:any="Todas";
  negociosService:NegociosService;
  negocio:any ={
    nome:"",
    telefone:"",
    lat:"",
    long:"",
    endereco:"",
    descricao:"",
    categoria:"0",
  };
  formValid:boolean = false;
  formValid2:boolean = false;
  enderSelect:boolean = false;
  enderSelectEdit:boolean = false;
  flagEditNegocio:boolean=false;
  session:Session = new Session();
  listNegocios:any=[];
  coordenadas:any={lat:"",long:""}
  meuNegocioUser:any={
    nome:"",
    telefone:"",
    lat:"",
    long:"",
    endereco:"",
    descricao:"",
    categoria:"0",
  };
  
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  
  handleAddressChange(event){
      console.log(event.geometry.location.lat());
      console.log(event.geometry.location.lng());
      let value:any = document.getElementById("endereco");
      this.negocio.endereco = value.value;
      this.negocio.lat = event.geometry.location.lat();
      this.negocio.long = event.geometry.location.lng();
      this.enderSelect = true;
      console.log(event.geometry);  
  }

  handleAddressChangeEdit(event){
    console.log(event.geometry.location.lat());
    console.log(event.geometry.location.lng());
    let value:any = document.getElementById("enderecoEdit");
    this.meuNegocioUser.endereco = value.value;
    this.meuNegocioUser.lat = event.geometry.location.lat();
    this.meuNegocioUser.long = event.geometry.location.lng();
    this.enderSelectEdit = true;
    console.log(event.geometry);  
}

  options:any = {
    types: [],
    componentRestrictions: { country: 'BR' }
  };

  constructor(private http: Http, private router: Router) {
    this.negociosService = new NegociosService(this.http);
    this.meuNegocio();
      
  }
  
  ngOnInit(){
    this.openLoading("Buscando negócios proximos à você...");

    this.getLocation((resp)=>{
      if(resp){
        this.coordenadas.lat = resp.latitude;
        this.coordenadas.long = resp.longitude;
        this.negociosService.listNegocio(resp.latitude,resp.longitude,(result)=>{
          console.log(result);
          if(result) this.listNegocios = result;
        });
      }
      this.closeLoading("");
    });


    $('.modal').modal({
      dismissible: false, // Modal can be dismissed by clicking outside of the modal
      opacity: .5, // Opacity of modal background
      inDuration: 300, // Transition in duration
      outDuration: 200, // Transition out duration
      ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
      },
      complete: function() {  } // Callback for Modal close
    });
    var form:any = document.querySelector("#formNegocio");
    var form2:any = document.querySelector("#formNegocioEdit");
    this.formValid = form.checkValidity();
    this.formValid2 = form2.checkValidity();
    window.addEventListener("keydown", (ev)=>{
      setTimeout(()=>{
        this.formValid = form.checkValidity();
        this.formValid2 = form2.checkValidity();
        // console.log(this.formValid);
        // console.log(this.enderSelect);
      },500)
      
    }, false);
  }
  getLocation(callback){
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((result)=>{
        console.log(result);
        callback(result.coords);
      },(error)=>{
        console.log(error);
        if(error.code==1){
          this.alert.openMsg("Para exibir os negócios próximos a você, é necessário que permita a sua geolocalização.",10000,"erro");
        }
        callback(false);
      });
    }else{
      callback(false);
    }
  }

  divulgar(){
    this.openNegocio();
  }
  meuNegocio(){
    this.negociosService.meuNegocio((result)=>{
      if(result){
        this.flagEditNegocio = true;
        this.meuNegocioUser = result;
      }else{
        this.flagEditNegocio = false;
      }
    });
  }
  criarNegocio(){
    this.openLoading("Cadastrando meu negócio...");
    console.log(this.negocio);
    this.negociosService.sendNegocio(this.negocio,(result)=>{
      if(result){
        this.closeNegocio();
      }
      this.closeLoading("");
      this.filtroNegocio(0);
    });

  }

  editarNegocio(){
    this.openLoading("Editando meu negócio...");
    console.log(this.meuNegocioUser);
    this.negociosService.editNegocio(this.meuNegocioUser,(result)=>{
      if(result){
        this.closeNegocioEdit();
      }
      this.closeLoading("");
      this.filtroNegocio(0);
    });
  }

  editInfo(){
    this.meuNegocio();
    this.enderSelectEdit = true;
    $('#modal-negocio-edit').modal('open');
    console.log(this.meuNegocioUser);
  }
  openNegocio(){
    if(this.session.authenticated("vm_tk")){
      $('#modal-negocio').modal('open');
      // this.openLoading("Enviando mensagem...");

    }else{
      var event = new CustomEvent(
        "openLogin",
        { detail: { 'msg': ""}}
      );
      document.dispatchEvent(event);
    }    
  }
  closeNegocio(){
    $('#modal-negocio').modal('close');
  }
  closeNegocioEdit(){
    $('#modal-negocio-edit').modal('close');
  }
  closeEnderec(){
    this.enderSelect = false;
    this.formValid = false;
    let value:any = document.getElementById("endereco");
    value.value="";
    this.negocio.lat="";
  }
  closeEnderecEdit(){
    this.enderSelectEdit = false;
    this.formValid2 = false;
    let value:any = document.getElementById("enderecoEdit");
    value.value="";
    this.meuNegocioUser.lat="";
  }
  openLoading(msg){
    var event = new CustomEvent(
      "openLoading",
      { detail: { 'msg': msg}}
    );
    document.dispatchEvent(event);
  }

  closeLoading(msg){
    var event = new CustomEvent(
      "closeLoading",
      { detail: { 'msg': msg}}
    );
    document.dispatchEvent(event);
  }

  filtroNegocio(idx){
    this.filtroSelect = this.categorias[idx].name;
    this.negociosService.filtroNegocio(this.coordenadas.lat,this.coordenadas.long,idx,(result)=>{
      console.log(result);
      if(result) this.listNegocios = result;
    });
  }
}

