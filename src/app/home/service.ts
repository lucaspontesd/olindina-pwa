import { Headers, RequestOptions } from '@angular/http';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { AlertDom } from '../utils/alert/alert';
import { Values } from '../utils/values';
import { Session } from '../utils/session';

declare var jquery:any;
declare var $ :any;
declare var Materialize:any;


export class HomeService {
    http:any;
    alert:any;
    session:Session = new Session();
    constructor(http) {
      this.http = http;
      this.alert = new AlertDom();
    }
    sendChat(chat,callback){
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append("Authorization","Bearer "+this.session.getSession("vm_tk"));

      console.log(chat);

      this.http.post(Values.URL+"chat",JSON.stringify(chat),{
        headers: headers}
      ).subscribe(res => {
        let result = res.json();
        console.log("chat",res.json());
        if(!result.error){
          callback(true);
        }
        callback(false);

       }, (err) => {
         console.log(err);
        //  let resp = JSON.parse(err._body);
         this.alert.openMsg("Ocorreu um erro interno.",3000,"erro");
         callback(false);
       });
    }
    listChat(lat,long,raio,data,callback){
      console.log(this.session.getSession("vm_tk"));
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      if(this.session.getSession("vm_tk")){
        headers.append("Authorization","Bearer "+this.session.getSession("vm_tk"));
      }
      var d = new Date,
      dformat = [d.getFullYear()]+"-"+
                (d.getMonth()+1)+"-"+
                d.getDate()+' '+
                [d.getHours(),
                d.getMinutes(),
                d.getSeconds()].join(':');
      let url=Values.URL+"chat?lat="+lat+"&long="+long+"&radius="+raio;
      if(data!="0"){
        url = url+"&date="+data;
      } 
      console.log(url);
      this.http.get(url,{
        headers: headers}
      ).subscribe(res => {
        let data = res.json();
        if(!data.error){
          callback(data.result);
        }
        callback(false);
       
       }, (err) => {
         console.log(err);
         callback(false);
       });
    }

}
