import { Component, OnInit, ViewChild } from '@angular/core';
import { HomeService } from './service';
import { Session } from '../utils/session';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { Select2OptionData } from 'ng-select2';
import { Options } from 'select2';
// import { Values } from '../utils/values';
import { AlertDom } from '../utils/alert/alert';

import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

declare var navigator:any;
declare var $ :any;
declare var window :any;
declare var SimpleBar:any;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  alert = new AlertDom();
  public exampleData: Array<Select2OptionData>;
  public options: Options;
  public ajaxOptions: any;
  raio:number = 1.0;
  session:Session = new Session();
  msg:any = {
    msg: "",
    lat: "",
    long: "",
    user_id_dest: "",
  };
  msgAviso:any="Buscando mensagens próximas há você...";
  resp:any;
  lastData:any="0";
  homeService:HomeService;
  listMsg:any=[];
  idUser:number=0;
  interval:any;
  geolocation:any={
    latitude:"",
    longitude:""
  }
  constructor(private http: Http, private router: Router,private spinner: NgxSpinnerService) {
    this.homeService = new HomeService(this.http);
    this.getLocation((geolocation)=>{
      if(geolocation){
        this.geolocation.latitude = geolocation.latitude;
        this.geolocation.longitude = geolocation.longitude;
        this.listChat();
      }
    });
    
    if(localStorage.getItem("vm_user")){
      this.idUser = JSON.parse(localStorage.getItem("vm_user")).id;
    }
    
  }
  
  ngOnInit(){
     $('.modal').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        inDuration: 300, // Transition in duration
        outDuration: 200, // Transition out duration
        ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
        },
        complete: function() {  } // Callback for Modal close
      }
    );
    var input = document.getElementById("perguntar");
    input.addEventListener("keyup", (event)=>{
      if (event.keyCode === 13) {
        event.preventDefault();
        this.sendMsg();
      }
    });
  
  }
  ngOnDestroy(){
    clearInterval(this.interval);
  }

  listChat(){
    this.interval = setInterval(()=>{
      if(!document.querySelector("app-home")){
        clearInterval(this.interval);
      }
      if(localStorage.getItem("vm_user")){
        this.idUser = JSON.parse(localStorage.getItem("vm_user")).id;
      }
      this.homeService.listChat(this.geolocation.latitude,this.geolocation.longitude,this.raio,this.lastData,(data)=>{
        if(data.length){
          // this.listMsg = data;
          this.listMsg  = this.listMsg.concat(data);
          if(this.listMsg.length>0){
            this.lastData = this.listMsg[this.listMsg.length-1].created_at;
            console.log(this.lastData);
          }
        }else{
          this.msgAviso="Não há mensagens próximas a você, aumente o valor do raio no filtro."
        }

        console.log(this.listMsg);
        console.log(data);
      });
      this.closeLoading("");
    },3000)


  }

  escolhaRaio(raio){
    // this.openLoading("Carregando novo raio de mensagens...");
    this.raio = raio;
  }

  sendMsg(){
    
    if(this.session.authenticated("vm_tk")){
      this.resp = false;
      this.openLoading("Enviando mensagem...");
      this.getLocation((geolocation)=>{
        if(geolocation){
          this.msg.lat = geolocation.latitude;
          this.msg.long = geolocation.longitude;
          console.log("Retorna aq marilene");
          console.log(geolocation);
          console.log(this.msg);
          this.homeService.sendChat(this.msg,(result)=>{
            if(result){
              this.closeResp();
              this.msg.msg="";
              
            }
            this.closeLoading("");
          });
        }else{
          this.closeLoading("");
        }
        
      });


    }else{
      var event = new CustomEvent(
        "openLogin",
        { detail: { 'msg': ""}}
      );
      document.dispatchEvent(event);
    }
    
  }
 getLocation(callback){
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((result)=>{
        console.log(result);
        callback(result.coords);
      },(error)=>{
        console.log(error);
        if(error.code==1){
          this.alert.openMsg("Para enviar mensagem é necessário que permita a sua geolocalização.",10000,"erro");
          this.msgAviso="Para exibirmos as mensagens proxímas a você, é necessário que permita a sua geolocalização.";
        }
        callback(false);
      });
    }else{
      callback(false);
    }
  }

  responder(item){
    console.log(item);
    this.resp = item;
    this.msg.user_id_dest = item.user_send.id;
    console.log(this.msg);
    document.getElementById("perguntar").scrollIntoView();
    document.getElementById("perguntar").focus();
  }

  closeResp(){
    this.resp = null;
    this.msg.user_id_dest="";
  }

  openLoading(msg){
    var event = new CustomEvent(
      "openLoading",
      { detail: { 'msg': msg}}
    );
    document.dispatchEvent(event);
  }

  closeLoading(msg){
    var event = new CustomEvent(
      "closeLoading",
      { detail: { 'msg': msg}}
    );
    document.dispatchEvent(event);
  }
  
  
  
}
