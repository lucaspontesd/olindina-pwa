import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

declare var jquery:any;
declare var $ :any;
declare var L;
declare var LeafIcon;
declare var window :any;
declare var SimpleBar:any;
declare var google:any;
declare var navigator:any;

@Component({
  selector: 'app-dados',
  templateUrl: './dados.component.html',
  styleUrls: ['./dados.component.css']
})
export class DadosComponent implements OnInit {
   
  
  constructor(private http: Http, private router: Router) {
    
      
  }
  
  ngOnInit(){
    // this.openLoading("Buscando negócios proximos à você...");
   

  }
  openLoading(msg){
    var event = new CustomEvent(
      "openLoading",
      { detail: { 'msg': msg}}
    );
    document.dispatchEvent(event);
  }

  closeLoading(msg){
    var event = new CustomEvent(
      "closeLoading",
      { detail: { 'msg': msg}}
    );
    document.dispatchEvent(event);
  }
}

