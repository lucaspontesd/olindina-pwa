import { Component, OnInit, ViewChild,HostListener } from '@angular/core';
import { Http, Response } from '@angular/http';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Session } from '../utils/session';
import { MenuService } from './service';
import { AlertDom } from '../utils/alert/alert';


import { NgxSpinnerService } from "ngx-spinner";


declare var jquery:any;
declare var $ :any;
declare var SimpleBar :any;
declare var navigator:any;

@Component({
  selector: 'menu',
  templateUrl: 'menu.component.html',
  styleUrls: ['menu.component.css']
})
export class MenuComponent {
  alert = new AlertDom();
  menuService:MenuService;
  session:Session = new Session();
  msgLoading:any="";
  user:any ={
    name:"",
    data_nascimento:"",
    email:"",
    password:"",
    rptSenha:""
  };
  login:any ={
    email:"",
    password:""
  };
  username:string="teste";
  logado:boolean = false;
  constructor(private http:Http, private router: Router,private spinner: NgxSpinnerService) {
    this.menuService = new MenuService(this.http);
    if(this.session.authenticated("vm_tk")){
      this.logado = true;
      this.username = this.session.getUser().name.split(" ")[0];
    }
    console.log(this.session.authenticated("vm_tk"));
   
  }
  ngOnInit(){
    // new SimpleBar(document.getElementById('lista-desafios'));
    $('.modal').modal({
         dismissible: true, // Modal can be dismissed by clicking outside of the modal
         opacity: .5, // Opacity of modal background
         inDuration: 300, // Transition in duration
         outDuration: 200, // Transition out duration
         ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
         },
         complete: function() {  } // Callback for Modal close
       }
     );
    $('button.navbar-toggle').sideNav({
       menuWidth: 300,
       edge: 'right',
       closeOnClick: true,
       draggable: true,
       onOpen: function(el) { },
       onClose: function(el) { },
     });
    
  }

  verificaLogin(){
    if(!this.session.authenticated("vm_tk")){
      this.openLogin();
      this.alert.openMsg("É necessário que acesse sua conta antes.",5000,"aviso");
    }else{
      
    }
  }
  
  fazerCadastro(){
    this.closeLogin();
    setTimeout(()=>{
      this.openRegister();
    },500)
  }

  openRegister(){
    $('#modal-register').modal('open');
  }

  openModalSenha(){
    this.closeLogin();
    setTimeout(()=>{
      $('#modal-senha').modal('open');
    },500)
  }
  closeModalSenha(){
    $('#modal-senha').modal('close');
  }
  closeRegister(){
    $('#modal-register').modal('close');
  }
  openLogin(){
    $('#modal-login').modal('open');
  }
  closeLogin(){
    $('#modal-login').modal('close');
  }
  criarUser(){
    console.log(this.user);
    if(this.verifyPassword()){
      this.menuService.registerUsuario(this.user,(user)=>{
        if(user){
          this.username = user.name.split(" ")[0];
          this.logado = true;
          this.closeRegister();
        }
      });
    }else{
      this.alert.openMsg("Repita sua senha corretamente.",3000,"erro");
    }
  }

  recuperarSenha(){
    this.msgLoading = "Enviando nova senha ao seu email...";
    this.spinner.show();
    this.menuService.recuperarSenha(this.login,(result)=>{
      this.spinner.hide();
      if(result){
        // this.username = user.name.split(" ")[0];
        // this.logado = true;
        // this.closeRegister();
      }
    });
  }

  verifyPassword(){
    return this.user.password==this.user.rptSenha;
  }
  fazerLogin(){
    this.menuService.loginUsuario(this.login,(user)=>{
      if(user){
        this.username = user.name.split(" ")[0];
        this.logado = true;
        this.closeLogin();
      }
      
    });
  }
  toggleHandler(){
    if($("#menu-hamburg").attr("menu")=="open"){
      $("#menu-header").css("zIndex","1012");
      $("#menu-hamburg").attr("menu","close");
      
    }else{
      setTimeout(()=>{
        $("#menu-hamburg").attr("menu","open");
      },300);
      $("#menu-header").css("zIndex","1005");
    }

  }
  @HostListener('document:openLogin', ['$event', '$event.detail.msg'])
  eventLogin(event, msg){
    this.verificaLogin();
  }

  @HostListener('document:openLoading', ['$event', '$event.detail.msg'])
  openLoading(event, msg){
    this.msgLoading = msg;
    this.spinner.show();
  }

  @HostListener('document:closeLoading', ['$event', '$event.detail.msg'])
  closeLoading(event, msg){
    this.msgLoading = msg;
    this.spinner.hide();
  }

  ngOnChange(){}
  logout(){
    localStorage.clear();
    this.session.deleteSession("vm_tk");
    this.logado = false;
    this.login = {email:"",password:""};
  }
  
  
  
}
