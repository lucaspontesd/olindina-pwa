import { Headers, RequestOptions } from '@angular/http';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { AlertDom } from '../utils/alert/alert';
import { Values } from '../utils/values';
import { Session } from '../utils/session';

declare var jquery:any;
declare var $ :any;
declare var Materialize:any;


export class MenuService {
    http:any;
    alert:any;
    session:Session = new Session();

    constructor(http) {
      this.http = http;
      this.alert = new AlertDom();

    }

    loginUsuario(usuario,callback){
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log(usuario);
       this.http.post(Values.URL+"login",JSON.stringify(usuario),{
         headers: headers}
       ).subscribe(res => {
           console.log("login",res.json());
        
           localStorage.setItem("vm_user",JSON.stringify(res.json().user));
           this.session.criarSession("vm_tk",res.json().token,"2038-01-19 04:14:07");
           this.alert.openMsg("Conta acessada com sucesso",3000,"sucesso");
           callback(res.json().user);
       
       }, (err) => {
         console.log(err);
         let resp = JSON.parse(err._body);
         this.alert.openMsg(resp.error,3000,"erro");
         callback(false);
       });
    }

    registerUsuario(usuario,callback){
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log(usuario);
       this.http.post(Values.URL+"register",JSON.stringify(usuario),{
         headers: headers}
       ).subscribe(res => {
           console.log("register",res.json());
        
           localStorage.setItem("vm_user",JSON.stringify(res.json().user));
           this.session.criarSession("vm_tk",res.json().token,"2038-01-19 04:14:07");
           this.alert.openMsg("Cadastro realizado com sucesso",3000,"sucesso");
           callback(res.json().user);
       
       }, (err) => {
         
         let resp = err._body;
         if(resp.indexOf("The email has already been taken")!=-1){
          this.alert.openMsg("Esse email já esta cadastrado.",3000,"erro");
         }else{
          this.alert.openMsg("Ocorreu um erro interno. Por favor tente mais tarde.",3000,"erro");
         }
         
         callback(false);
       });
    }

    recuperarSenha(usuario,callback){
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log(usuario);
       this.http.post(Values.URL+"recuperarSenha",JSON.stringify(usuario),{
         headers: headers}
       ).subscribe(res => {
        console.log(res.json());
         if(!res.json().error){
           this.alert.openMsg(res.json().msg,3000,"sucesso");
           callback(true);
         }else{
           this.alert.openMsg(res.json().msg,3000,"erro");
           callback(false);
         }
       }, (err) => {
         this.alert.openMsg("Ocorreu um erro interno.",3000,"erro");
         callback(false);
       });
    }

}
