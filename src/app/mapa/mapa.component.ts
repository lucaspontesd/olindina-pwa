import { Component, OnInit, ViewChild } from '@angular/core';
import { Session } from '../utils/session';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route } from '@angular/router';
import { MapaService } from './service';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

declare var jquery:any;
declare var $ :any;
declare var L;
declare var LeafIcon;
declare var window :any;
declare var SimpleBar:any;
declare var google:any;
declare var navigator:any;

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {
  mapaService:MapaService;
  listEquipamentos:any=[];
  listEquipamentosFilter:any=[];
  mapMarkers:any=[];
  map:any;
  meuLocal:any = {
    url: {},
    scaledSize: {
      height: 40,
      width: 30
    },
    latitude:0,
    longitude:0
  };
  constructor(private http: Http, private router: Router) {
      this.mapaService = new MapaService(this.http);
      
  }

  ngOnInit(){

    // this.mapaService.getEquipamentos((result)=>{

    //   this.listEquipamentos = result;
    //   for(let item of this.listEquipamentos){
    //     if(item.tipo=="ARENINHA"){
    //       item.url = {
    //             url: './assets/assets/img/pin/icones-03.png',
    //             scaledSize: {
    //               height: 40,
    //               width: 30
    //             }
    //       }
    //     }else if(item.tipo=="UPA"){
    //       item.url = {
    //             url: './assets/assets/img/pin/icones-02.png',
    //             scaledSize: {
    //               height: 40,
    //               width: 30
    //             }
    //       }
    //     }else if(item.tipo=="POLICLINICA"){
    //       item.url = {
    //             url: './assets/assets/img/pin/icones-04.png',
    //             scaledSize: {
    //               height: 40,
    //               width: 30
    //             }
    //       }
    //     }else if(item.tipo=="CAPS"){
    //       item.url = {
    //             url: './assets/assets/img/pin/icones-05.png',
    //             scaledSize: {
    //               height: 40,
    //               width: 30
    //             }
    //       }
    //     }else if(item.tipo=="1"){
    //       item.url = {
    //             url: './assets/assets/img/pin/icones-03.png',
    //             scaledSize: {
    //               height: 45,
    //               width: 35
    //             }
    //       }
    //     }
    //   }

      
      this.listEquipamentosFilter = this.listEquipamentos;

      $('.tooltipped').tooltip({delay: 50});
      this.map = L.map("map").setView([-3.763776, -38.532921], 12);
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(this.map);
      var searchControl = new L.esri.Controls.Geosearch().addTo(this.map);

      // var results = L.LayerGroup().addTo(this.map);
      searchControl.on('results', function(data){
        // results.clearLayers();
        for (var i = data.results.length - 1; i >= 0; i--) {
          // results.addLayer(L.marker(data.results[i].latlng));
          console.log(data.results[i].latlng);
        }
      });
  



      this.addMarkerIcon(this.listEquipamentosFilter);

      this.getLocation();


    // });
   

  }
  addMarkerIcon(listEquipamentosFilter){
    for(var i = 0; i < this.mapMarkers.length; i++){
      this.map.removeLayer(this.mapMarkers[i]);
    }

    for(let item of listEquipamentosFilter){
      // console.log("MARILENE", item);
      var icon = L.icon({iconUrl: item.url.url, iconSize:[item.url.scaledSize.width, item.url.scaledSize.height]});
      var marker = L.marker([item.latitude, item.longitude], {icon: icon}).addTo(this.map).bindPopup(item.nome);
      
      this.mapMarkers.push(marker);
    }
    
  }
  getLocation(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((result)=>{
          this.meuLocal.latitude = result.coords.latitude;
          this.meuLocal.longitude = result.coords.longitude;
          this.meuLocal.url = {
                url: 'assets/assets/img/icones-01.png',
                scaledSize: {
                  height: 40,
                  width: 30
                }
          };

          var icon = L.icon({iconUrl: this.meuLocal.url.url, iconSize:[this.meuLocal.url.scaledSize.width, this.meuLocal.url.scaledSize.height]});
          var marker = L.marker([this.meuLocal.latitude, this.meuLocal.longitude], {icon: icon}).addTo(this.map).bindPopup("Sua localização");
          this.map.setView(new L.LatLng(result.coords.latitude, result.coords.longitude), 15);
          console.log(result);
        });
    }
  }
  filterEquipamento(tipo){
    let size = this.listEquipamentosFilter.length;
    var arrayFilter = [];
    for(let idx = 0; idx<size; idx++){
      if(tipo==2 && this.listEquipamentosFilter[idx].tipo == "UPA"){
        arrayFilter.push(this.listEquipamentosFilter[idx]);
      }
      else if(tipo==3 && this.listEquipamentosFilter[idx].tipo == "ARENINHA"){
        arrayFilter.push(this.listEquipamentosFilter[idx]);
      }
      else if(tipo==6 && this.listEquipamentosFilter[idx].tipo == "1"){
        arrayFilter.push(this.listEquipamentosFilter[idx]);
      }
      else if(tipo==4 && this.listEquipamentosFilter[idx].tipo == "POLICLINICA"){
        arrayFilter.push(this.listEquipamentosFilter[idx]);
      }
      else if(tipo==5 && this.listEquipamentosFilter[idx].tipo == "CAPS"){
        arrayFilter.push(this.listEquipamentosFilter[idx]);
      }
    }
    this.listEquipamentos = arrayFilter;
    if(tipo==1){
      this.listEquipamentos = this.listEquipamentosFilter;
    }

    this.addMarkerIcon(arrayFilter);
  }
}
