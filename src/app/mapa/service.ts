import { Headers, RequestOptions } from '@angular/http';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { AlertDom } from '../utils/alert/alert';
import { Values } from '../utils/values';
declare var jquery:any;
declare var $ :any;
declare var Materialize:any;


export class MapaService {
    http:any;
    alert:any;
    constructor(http) {
      this.http = http;
      this.alert = new AlertDom();
    }

    getEquipamentos(callback){
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append(Values.API_KEY_ATTR,Values.API_KEY);
       this.http.get(Values.URL+"equipamentos/getEquipamentos",{
         headers: headers}
       ).subscribe(res => {
        // console.log(res.json());
        callback(res.json().objeto);
       }, (err) => {
        console.log(err.json());
        callback(err.json());
       });
    }



}
